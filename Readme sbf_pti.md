# "Sbf_pti"

Proyek Sbf_pti ini merupakan proyek akhir dari SBF PTI Fasilkom yang merupakan web app menggunakan django sebagai library dan juga menggunakan HTML dan CSS. Untuk menampilkan laman secara maksimal disarankan membuka laman dengan ukuran full screen.

## Daftar isi

Pada proyek ini terdapat:
* Halaman Portfolio
* Halaman Comments yang juga terdapat like
* Halaman Friends
* User session


### Yang digunakan

* Python
* HTML
* CSS & CSS 3

## Menggunakan

* Micro celearfix from nicolasgallagher.com
* CSS reset from meyerweb.com
* Inspirasi dunia maya
* dan lain sebagainya

## Dibuat oleh

**Tolhas Parulian Jonathan** dengan berbagai inspirasi

## Acknowledgments

* Kak Jojo
* The Net Ninja
* Teman-teman SBF PTI
* YouTube
* Google
* Lingkungan sekitar
* Kearifan budaya lokal
* Dan lain sebagainya