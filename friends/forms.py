from . import models
from django import forms
from django.contrib.auth.models import User

class AddFriend(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "smallfields",
                "required" : True,
                "placeholder":"Nama kamu",
                }))

    gender = forms.ChoiceField(choices=(('Male', "Male"), ('Female', "Female")))

    tempatlahir = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "smallfields",
                "required" : True,
                "placeholder":"Tempat lahir kamu",
                }))

    tanggallahir = forms.DateField(widget=forms.SelectDateWidget(attrs={
                "class" : "datefield",
                "required" : True,
                }, years=[2019-i for i in range(100)]))

    line = forms.CharField(widget=forms.TextInput(attrs={
                "class" : "smallfields",
                "required" : True,
                "placeholder":"id line kamu",
                }))

    deskripsi = forms.CharField(widget=forms.Textarea(attrs={
                "class" : "bigfields",
                "required" : True,
                "placeholder":"Deskripsi diri kamu",
                }))
    class Meta:
        model=models.Friend
        fields=["name", "gender", "tempatlahir", "tanggallahir", "line", "deskripsi", "avatar"]