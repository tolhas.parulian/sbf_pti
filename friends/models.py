from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions

def validate_image(picture):
    w, h = get_image_dimensions(picture)
    if w > 250:
        raise ValidationError("The image is")
    if h > 340:
        raise ValidationError("The image is")
    return picture


class Friend(models.Model):
    username = models.ForeignKey(User, on_delete=models.CASCADE, null=True, )
    name = models.CharField(max_length=50)
    gender= models.TextField(choices=(('Male',"Male"),('Female', "Female")))
    tempatlahir = models.CharField(max_length=50)
    tanggallahir = models.DateField()
    line = models.CharField(max_length=30)
    deskripsi = models.TextField(null=True, default="I'm friend of Tolhas")
    avatar = models.ImageField(blank=True, null=True, validators=[validate_image], help_text='Upload gambar dengan ukuran maksimum 250x340 piksel.')

    def __str__(self):
        return "{} - {}".format(self.username, self.name)

    # def avatar_consider(self):
    #     if self.gender=='Male':
    #         return str(male.png)
    #     else:
    #         return str(female.png)