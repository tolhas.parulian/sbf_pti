from django.shortcuts import render, redirect
from . import forms
from .models import Friend
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

# Create your views here.

def friend_clarification(live_user):
    for friend in Friend.objects.all():
        if friend.username == live_user: return True
    return False


def friends_list(request):
    friend_response = {
        "friends_object": Friend.objects.all().order_by("name"),
        "friends_object_total": len(Friend.objects.all()),
        "existance": friend_clarification(request.user)
    }
    return render(request, "friends/friends-list.html", friend_response)

def friends_detail(request, slug):
    myfriend = Friend.objects.get(username=User.objects.get(username=slug))
    return render(request, "friends/friends-detail.html", {"myfriend":myfriend})


@login_required(login_url="accounts:login")
def friends_add(request):
    live_user=request.user
    for friend in Friend.objects.all():
        if friend.username == live_user:
            return redirect("friends:detail", request.user)
    if request.method == "POST":
        form=forms.AddFriend(request.POST, request.FILES)
        if form.is_valid():
            #save friend to
            myfriend = form.save(commit=False)
            myfriend.username = request.user
            myfriend.name = form.cleaned_data["name"].capitalize()
            myfriend.save()
            return redirect ("friends:list")
        else:
            for friend in Friend.objects.all():
                if friend.username == live_user:
                    return redirect("friends:detail", request.user)
    form=forms.AddFriend()
    return render(request, "friends/friends-add.html", {"form": form} )



