from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'portfolio_page/content.html')

def index_part1(request):
    return render(request, 'portfolio_page/content.html')